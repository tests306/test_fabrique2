from .models import MESSAGE_STATUS_CHOICES


def get_message_status_choices():
    return [status[0] for status in MESSAGE_STATUS_CHOICES]


def set_statuses_with_zero_messages(message_stats):
    for status in get_message_status_choices():
        if status not in message_stats:
            message_stats[status] = 0
    return message_stats
