from django.db.models.signals import post_save
from django.dispatch import receiver
from main.services import process_mailing
from main.models import Mailing


@receiver(post_save, sender=Mailing)
def process_mailing_handler(sender, instance, **kwargs):
    process_mailing(instance)

