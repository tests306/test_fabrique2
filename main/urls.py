from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path("client/", views.ClientCreateAPIView.as_view(), name='client-create'),
    path("client/<int:pk>", views.ClientUpdateDeleteAPIView.as_view(), name='client-single'),
    path("mailing/", views.MailingCreateAPIView.as_view(), name='mailing-create'),
    path("mailing/<int:pk>", views.MailingUpdateDeleteAPIView.as_view(), name='mailing-single'),
    path("mailing/statistics/<int:pk>", views.MailingStatistics.as_view(), name='mailing-statistic'),
    path("mailing/statistics/", views.AllMailingStatistics.as_view()),
    path(
        "docs/",
        TemplateView.as_view(template_name="swagger-ui.html"),
        name="swagger-ui",
    ),
]
