from main.funcs import get_message_status_choices, set_statuses_with_zero_messages


def test_get_message_status_choices():
    choices = ["process", "sent", "error"]
    assert get_message_status_choices() == choices


def test_set_statuses_with_zero_messages():
    stats = {"process": 2}
    expected_stats = {"process": 2, "sent": 0, "error": 0}
    assert set_statuses_with_zero_messages(stats) == expected_stats
