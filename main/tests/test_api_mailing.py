import pytest
from rest_framework import status
from rest_framework.test import APIClient
from main.models import Mailing
from django.urls import reverse
from datetime import datetime, timedelta
from django.db.models.signals import post_save
from main.signals.handlers import process_mailing_handler

client = APIClient()


def datetime_to_string(datetime_object):
    return datetime_object.strftime("%Y-%m-%dT%H:%M:%S.%f")


@pytest.fixture(autouse=True)
def disable_mailing_processing():
    post_save.disconnect(process_mailing_handler, sender=Mailing)
    yield
    post_save.connect(process_mailing_handler, sender=Mailing)


@pytest.mark.django_db
def test_create_mailing(mailing_data):
    response = client.post(reverse("mailing-create"), mailing_data)
    assert response.status_code == status.HTTP_201_CREATED
    mailing = Mailing.objects.first()
    assert mailing is not None


@pytest.mark.django_db
def test_retrieve_mailing(mailing_create):
    response = client.get(reverse("mailing-single", kwargs={"pk": mailing_create.id}))
    assert response.status_code == status.HTTP_200_OK
    mailing = response.data
    assert len(mailing) == 6
    assert mailing["start_time"] == datetime_to_string(mailing_create.start_time)
    assert mailing["message"] == mailing_create.message
    assert mailing["filter_choice"] == mailing_create.filter_choice
    assert mailing["filter"] == mailing_create.filter
    assert mailing["end_time"] == datetime_to_string(mailing_create.end_time)


@pytest.mark.django_db
def test_update_mailing(mailing_create):
    start_time = datetime.now()
    end_time = start_time + timedelta(hours=1)
    updated_data = {
        "start_time": start_time,
        "message": "Updated message",
        "filter_choice": "tag",
        "filter": "second",
        "end_time": end_time,
    }
    response = client.put(
        reverse("mailing-single", kwargs={"pk": mailing_create.id}), updated_data
    )
    assert response.status_code == status.HTTP_200_OK
    mailing = response.data
    assert mailing["start_time"] == datetime_to_string(updated_data["start_time"])
    assert mailing["message"] == updated_data["message"]
    assert mailing["filter_choice"] == updated_data["filter_choice"]
    assert mailing["filter"] == updated_data["filter"]
    assert mailing["end_time"] == datetime_to_string(updated_data["end_time"])


@pytest.mark.django_db
def test_patch_mailing(mailing_create):
    updated_data = {"message": "Patched message"}
    response = client.patch(
        reverse("mailing-single", kwargs={"pk": mailing_create.id}), updated_data
    )
    assert response.status_code == status.HTTP_200_OK
    mailing = response.data
    assert mailing["start_time"] == datetime_to_string(mailing_create.start_time)
    assert mailing["message"] == updated_data["message"]
    assert mailing["filter_choice"] == mailing_create.filter_choice
    assert mailing["filter"] == mailing_create.filter
    assert mailing["end_time"] == datetime_to_string(mailing_create.end_time)
