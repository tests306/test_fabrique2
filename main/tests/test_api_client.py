import pytest
from rest_framework import status
from rest_framework.test import APIClient
from main.models import Client
from django.urls import reverse

client = APIClient()


@pytest.mark.django_db
def test_create_client(client_data):
    response = client.post(reverse("client-create"), client_data)
    assert response.status_code == status.HTTP_201_CREATED
    mailing_client = Client.objects.first()
    assert mailing_client is not None


@pytest.mark.django_db
def test_retrieve_client(client_create):
    response = client.get(reverse("client-single", kwargs={"pk": client_create.id}))
    assert response.status_code == status.HTTP_200_OK
    mailing_client = response.data
    assert len(mailing_client) == 5
    assert mailing_client["phone_number"] == client_create.phone_number
    assert mailing_client["phone_code"] == client_create.phone_code
    assert mailing_client["tag"] == client_create.tag
    assert mailing_client["timezone"] == client_create.timezone


@pytest.mark.django_db
def test_update_client(client_create):
    updated_data = {
        "phone_number": "79164307296",
        "phone_code": "916",
        "tag": "first",
        "timezone": "+3",
    }
    response = client.put(
        reverse("client-single", kwargs={"pk": client_create.id}), updated_data
    )
    assert response.status_code == status.HTTP_200_OK
    mailing_client = response.data
    assert mailing_client["phone_number"] == updated_data["phone_number"]
    assert mailing_client["phone_code"] == updated_data["phone_code"]
    assert mailing_client["tag"] == updated_data["tag"]
    assert mailing_client["timezone"] == updated_data["timezone"]


@pytest.mark.django_db
def test_patch_client(client_create):
    updated_data = {"phone_number": "79164307296"}
    response = client.patch(
        reverse("client-single", kwargs={"pk": client_create.id}), updated_data
    )
    assert response.status_code == status.HTTP_200_OK
    mailing_client = response.data
    assert mailing_client["phone_number"] == updated_data["phone_number"]
    assert mailing_client["phone_code"] == client_create.phone_code
    assert mailing_client["tag"] == client_create.tag
    assert mailing_client["timezone"] == client_create.timezone
