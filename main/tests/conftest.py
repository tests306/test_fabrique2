import pytest
from main.models import Client, Mailing
from main.serializers import ClientSerializer, MailingSerializer
from datetime import datetime, timedelta


@pytest.fixture
def client_data():
    data = {
        "phone_number": "79309666950",
        "phone_code": "930",
        "tag": "first",
        "timezone": "+3",
    }
    return data


@pytest.fixture
def client_create(client_data):
    record = Client.objects.create(**client_data)
    return record


@pytest.fixture
def client_serializer(client_create):
    serializer = ClientSerializer(instance=client_create)
    return serializer


@pytest.fixture
def mailing_data():
    start_time = datetime.now()
    end_time = start_time + timedelta(hours=1)
    data = {
        "start_time": start_time,
        "message": "Message",
        "filter_choice": "tag",
        "filter": "first",
        "end_time": end_time,
    }
    return data


@pytest.fixture
def mailing_create(mailing_data):
    record = Mailing.objects.create(**mailing_data)
    return record


@pytest.fixture
def mailing_serializer(mailing_create):
    serializer = MailingSerializer(instance=mailing_create)
    return serializer
