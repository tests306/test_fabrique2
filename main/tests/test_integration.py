import pytest
from rest_framework import status
from rest_framework.test import APIClient
from main.models import Message
from django.urls import reverse
from datetime import datetime

client = APIClient()


@pytest.mark.celery(task_always_eager=True)
@pytest.mark.django_db(transaction=True)
def test_integration_statistics(
    celery_app, celery_worker, client_create, mailing_create
):
    response = client.get(
        reverse("mailing-statistic", kwargs={"pk": mailing_create.id})
    )
    assert response.status_code == status.HTTP_200_OK
    expected_result = {
        "id": mailing_create.id,
        "statistics": {"sent": 1, "process": 0, "error": 0},
    }
    assert (
        response.data == expected_result
    ), "Message has not been sent, possibly dispatching service is down"


@pytest.mark.celery(task_always_eager=True)
@pytest.mark.django_db(transaction=True)
def test_integration_message_processing(
    celery_app, celery_worker, client_create, mailing_create
):
    message = Message.objects.last()
    assert message is not None
    assert message.mailing == mailing_create
    assert (
        message.mailing_status == "sent"
    ), "Message has not been sent, possibly dispatching service is down"
    assert mailing_create.start_time < message.mailing_time < datetime.now()
