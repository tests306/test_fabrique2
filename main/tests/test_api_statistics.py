import pytest
from rest_framework import status
from rest_framework.test import APIClient
from main.models import Mailing, Message
from django.urls import reverse
from django.db.models.signals import post_save
from main.signals.handlers import process_mailing_handler

client = APIClient()


@pytest.fixture(autouse=True)
def disable_mailing_processing():
    post_save.disconnect(process_mailing_handler, sender=Mailing)
    yield
    post_save.connect(process_mailing_handler, sender=Mailing)


@pytest.mark.django_db
def test_api_statistics(client_create, mailing_create):
    Message.objects.create(
        mailing_status="sent", mailing=mailing_create, client=client_create
    )
    Message.objects.create(
        mailing_status="sent", mailing=mailing_create, client=client_create
    )
    Message.objects.create(
        mailing_status="process", mailing=mailing_create, client=client_create
    )
    response = client.get(
        reverse("mailing-statistic", kwargs={"pk": mailing_create.id})
    )
    assert response.status_code == status.HTTP_200_OK
    expected_result = {
        "id": mailing_create.id,
        "statistics": {"sent": 2, "process": 1, "error": 0},
    }
    assert response.data == expected_result
