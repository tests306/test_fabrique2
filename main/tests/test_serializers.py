import pytest
from django.db.models.signals import post_save
from main.signals.handlers import process_mailing_handler
from main.models import Mailing


def datetime_to_string(datetime_object):
    return datetime_object.strftime("%Y-%m-%dT%H:%M:%S.%f")


@pytest.fixture(autouse=True)
def disable_mailing_processing():
    post_save.disconnect(process_mailing_handler, sender=Mailing)
    yield
    post_save.connect(process_mailing_handler, sender=Mailing)


@pytest.mark.django_db
def test_client_serializer(client_serializer, client_create):
    serializer_data = client_serializer.data
    assert list(serializer_data.keys()) == [
        "id",
        "phone_number",
        "phone_code",
        "tag",
        "timezone",
    ]
    assert serializer_data["id"] == client_create.id
    assert serializer_data["phone_number"] == client_create.phone_number
    assert serializer_data["phone_code"] == client_create.phone_code
    assert serializer_data["tag"] == client_create.tag
    assert serializer_data["timezone"] == client_create.timezone


@pytest.mark.django_db
def test_mailing_serializer(mailing_serializer, mailing_create):
    serializer_data = mailing_serializer.data
    assert list(serializer_data.keys()) == [
        "id",
        "start_time",
        "message",
        "filter_choice",
        "filter",
        "end_time",
    ]
    assert serializer_data["id"] == mailing_create.id
    assert serializer_data["start_time"] == datetime_to_string(
        mailing_create.start_time
    )
    assert serializer_data["message"] == mailing_create.message
    assert serializer_data["filter_choice"] == mailing_create.filter_choice
    assert serializer_data["filter"] == mailing_create.filter
    assert serializer_data["end_time"] == datetime_to_string(mailing_create.end_time)
