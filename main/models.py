from django.core.validators import RegexValidator
from django.db import models


# Create your models here.

MAILING_FILTER_CHOICES = [
    ("code", "Phone number operator code"),
    ("tag", "Tag"),
]

MESSAGE_STATUS_CHOICES = [
    ("process", "In process"),
    ("sent", "Message has been sent"),
    ("error", "Mailing has been aborted"),
]


class Mailing(models.Model):
    start_time = models.DateTimeField()
    message = models.TextField()
    filter_choice = models.CharField(
        max_length=10, choices=MAILING_FILTER_CHOICES, null=True
    )
    filter = models.CharField(max_length=100, null=True)
    end_time = models.DateTimeField()


class Client(models.Model):
    phone_number = models.CharField(
        max_length=12,
        validators=[
            RegexValidator(regex=r"^7[\d]{10}$", message="Invalid phone number")
        ],
    )
    phone_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=100)
    timezone = models.CharField(
        max_length=30,
        validators=[
            RegexValidator(
                regex=r"^[+-]([1-9]|1[0-2])$", message="Invalid UTC Timezone"
            )
        ],
    )


class Message(models.Model):
    mailing_time = models.DateTimeField(null=True)
    mailing_status = models.CharField(
        max_length=20, choices=MESSAGE_STATUS_CHOICES, null=True
    )
    mailing = models.ForeignKey(
        Mailing, on_delete=models.SET_NULL, null=True, related_name="messages"
    )
    client = models.ForeignKey(Client, on_delete=models.SET_NULL, null=True)



