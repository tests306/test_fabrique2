from celery import shared_task
from datetime import datetime
import requests
from .models import Message
from celery.exceptions import SoftTimeLimitExceeded
from .consts import MESSAGE_ENDPOINT_HEADERS, MESSAGE_MAX_RETRIES, MESSAGE_ENDPOINT


@shared_task(bind=True, soft_time_limit=10, max_retries=MESSAGE_MAX_RETRIES - 1)
def send_message(self, message_id, client_phone, message_text, time_delta):
    json = {
        "id": message_id,
        "phone": client_phone,
        "text": message_text,
    }
    try:
        response = requests.post(
            f"{MESSAGE_ENDPOINT}{message_id}",
            json=json,
            headers=MESSAGE_ENDPOINT_HEADERS,
        )
    except SoftTimeLimitExceeded:
        self.retry(countdown=time_delta)
    else:
        if response.ok:
            message = Message.objects.get(id=message_id)
            message.mailing_status = "sent"
            message.mailing_time = datetime.now()
            message.save()
        else:
            self.retry(countdown=time_delta)
