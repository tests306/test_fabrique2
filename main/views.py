import datetime
from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.response import Response
from main.models import Client, Mailing
from main.serializers import ClientSerializer, MailingSerializer
from django.db.models import Count
from main.services import process_mailing
from main.funcs import set_statuses_with_zero_messages


class ClientCreateAPIView(generics.CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientUpdateDeleteAPIView(
    generics.RetrieveUpdateAPIView, generics.DestroyAPIView
):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingCreateAPIView(generics.CreateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.validated_data['end_time'] < datetime.datetime.now():
            return Response({"error": "Mailing end time is lower than current time"})
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class MailingUpdateDeleteAPIView(
    generics.RetrieveUpdateAPIView, generics.DestroyAPIView
):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingStatistics(APIView):
    def get(self, request, pk):
        try:
            mailing = Mailing.objects.get(id=pk)
        except Mailing.DoesNotExist:
            return Response({"error": "mailing with such id does not exist"})
        messages = mailing.messages.values("mailing_status").annotate(
            num_messages=Count("mailing_status")
        )
        message_stats = {
            status["mailing_status"]: status["num_messages"] for status in messages
        }
        message_stats = set_statuses_with_zero_messages(message_stats)
        data = {"id": mailing.id, "statistics": message_stats}
        return Response(data)


class AllMailingStatistics(APIView):
    def get(self, request):
        data = []
        mailings = Mailing.objects.all()
        for mailing in mailings:
            messages = mailing.messages.values("mailing_status").annotate(
                num_messages=Count("mailing_status")
            )
            message_stats = {
                status["mailing_status"]: status["num_messages"] for status in messages
            }
            message_stats = set_statuses_with_zero_messages(message_stats)
            mailing_stats = {"id": mailing.id, "statistics": message_stats}
            data.append(mailing_stats)
        return Response(data)
