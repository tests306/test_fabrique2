from datetime import datetime, timedelta
from celery import shared_task
from .models import Client, Mailing, Message
from .tasks import send_message
from .consts import MESSAGE_MAX_RETRIES


def process_mailing(mailing):
    if mailing.start_time <= datetime.now() < mailing.end_time:
        dispatch_mailing.s(mailing.id).apply_async()
    elif mailing.start_time > datetime.now():
        dispatch_mailing.s(mailing.id).apply_async(eta=mailing.start_time)


@shared_task
def dispatch_mailing(mailing_id):
    mailing = Mailing.objects.get(id=mailing_id)
    clients = Client.objects.filter(**{mailing.filter_choice: mailing.filter})
    bulk_messages = []
    for client in clients:
        message = Message(mailing=mailing, client=client, mailing_status="process")
        bulk_messages.append(message)
    db_messages = Message.objects.bulk_create(bulk_messages)
    for message, client in zip(db_messages, clients):
        time_delta = (
            mailing.end_time - datetime.now() - timedelta(seconds=10)
        ) / MESSAGE_MAX_RETRIES
        send_message.s(
            message.id, client.phone_number, mailing.message, time_delta.total_seconds()
        ).apply_async()
    finish_dispatching.s(mailing.id).apply_async(eta=mailing.end_time)


@shared_task
def finish_dispatching(mailing_id):
    mailing = Mailing.objects.get(id=mailing_id)
    mailing.messages.filter(mailing_status="process").update(mailing_status="error")
