from decouple import config

MESSAGE_MAX_RETRIES = 10
MESSAGE_ENDPOINT = "https://probe.fbrq.cloud/v1/send/"
MESSAGE_ENDPOINT_HEADERS = {"Authorization": f'Bearer {config("JWT_TOKEN")}'}
